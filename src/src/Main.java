package src;

public class Main
{

    public static void main(String[] args) {

    }
    public String helloName(String name)
    {	return ("Hello " + name + '!');	}


    
    public String makeAbba(String a, String b)
    {	return (a+b+b+a);	}


  
    public String makeTags(String tag, String word)
    {	return ('<' + tag + '>' + word + '<' + '/' + tag + '>');	}


  
    public String makeOutWord(String out, String word)
    {	return (out.substring(0, 2) + word + out.substring(2, 4));	}


   
    public String extraEnd(String str)
    {
        int len = str.length();
        String temp = str.substring(len-2, len);
        return (temp + temp + temp);
    }


   
    public String firstTwo(String str)
    {
        if(str.length() >= 3)
            return str.substring(0, 2);
        return str;
    }


  
    public String firstHalf(String str)
    {
        return str.substring(0, str.length()/2);
    }


  
    public String withoutEnd(String str)
    {	return str.substring(1, str.length()-1);	}


    
    public String comboString(String a, String b)
    {
        if(a.length() >= b.length())
            return b+a+b;
        return a+b+a;
    }


    
    public String nonStart(String a, String b)
    {	return (a.substring(1) + b.substring(1));	}


    public String left2(String str)
    {	return (str.substring(2) + str.substring(0, 2));}


  
    public String right2(String str)
    {
        int len = str.length()-2;
        return (str.substring(len) + str.substring(0, len));
    }

}

